package ru.sbrf.mbcsm.mock;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@Path("/list")
public class MbcsmCardPhoneListEndpoint {

    @Context
    private SecurityContext security;

    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_JSON})
    public String getCard(String body) throws IOException {
        System.out.println(body);
        return FileUtils.readFileToString(new File(getClass().getClassLoader().getResource("538597785.json").getFile()), StandardCharsets.UTF_8);
    }
    private String getRequestBody(HttpServletRequest req) throws IOException {
        return req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
    }
}
